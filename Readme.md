# Performance Support XAPI Library

Javascript Performance Support based API to simplify communication to an LRS and provide the tools to help aid in one's Performance.

***
## **Contents**
- [PeformanceSupportXAPI](#markdown-header-performancesupportxapijs)
- [Dependencies](#markdown-header-dependencies)
- [Including The Library in Your Application](#markdown-header-including-the-library-in-your-application)
- [Launching Configuration](#markdown-header-launching-configuration)
- [Instantiating the library](#markdown-header-instantiating-the-library)
- [Utility Functions](#markdown-header-utility-functions)
    - [Adding Extensions](#markdown-header-adding-extensions)
    - [Creating a Parent Object](#markdown-header-creating-a-parent-object)
    - [PerformanceSupportXAPI Constants](#markdown-header-performancesupportxapi-constants)
- [Initializing an Application](#markdown-header-initializing-an-application)   
- [Terminating an Application](#markdown-header-terminating-an-application)
- [Selecting a Checklist Item](#markdown-header-selecting-a-checklist-item)
- [Deselecting a Checklist Item](#markdown-header-deselecting-a-checklist-item)
- [Completing a Checklist](#markdown-header-completing-a-checklist)
- [Step-by-step Procedures](#markdown-header-step-by-step-procedures)
    - [Viewing a Step](#markdown-header-viewing-a-step)
    - [Viewing a Procedure](#markdown-header-viewing-a-procedure)
- [Searching for Terms](#markdown-header-searching-for-terms)
- [Selecting a Search Result](#markdown-header-selecting-a-search-result)
- [Menus & Menu Items - Accessed Menu Item](#markdown-header-menus-menu-items-accessed-menu-item)
    - [accessed](#markdown-header-performancesupportxapiaccessed-type-value-id-name-description-parent)
- [Like & Dislike Buttons](#markdown-header-like-dislike-buttons)
    - [liked](#markdown-header-performancesupportxapiliked-type-id-name-description-parent)
    - [disliked](#markdown-header-performancesupportxapidisliked-type-id-name-description-parent)
- [Files, Links, and Pages](#markdown-header-files-links-and-pages)
    - [opened](#markdown-header-performancesupportxapiopened-type-id-name-description-parent)
    - [closed](#markdown-header-performancesupportxapiclosed-type-id-name-description-parent)
    - [printed](#markdown-header-performancesupportxapiprinted-type-id-name-description-parent)
- [Uploading a File](#markdown-header-uploading-a-file)
    - [attachment](#markdown-header-performancesupportxapiattachment-value-usagetype-contenttype-display-description)  
    - [uploaded](#markdown-header-performancesupportxapiuploaded-id-name-description-attachment)

***
## **PeformanceSupportXAPI**
 This library can be included in web based or standalone application to simplify the process of connecting and communicating to an LRS.
 The Performance Support xAPI Library wraps the [xapiwrapper](https://github.com/adlnet/xAPIWrapper) and uses an instance of the xapiwrapper to help build and send xAPI statements to an LRS.
***
## **Dependencies**
In order to start using the library install git. Git is a version control system used to track changes in source code during development.

- Download and install Git here https://git-scm.com/   

Use git to clone the Performance Support xAPI Library. Open up an terminal window and run the below command. 
```
git clone https://bitbucket.org/netc-lms-lrs/veracity-xapi-libraries.git
```
***
## Including the library in your application
After cloning this repository, inside the lib directory you'll find the performancesupportxapi.js file.

Include the PerformanceSupportXAPI library by including it in a script tag.

```
<script type="text/javascript" src="veracity.umd.production.min.js"></script>
```
Use npm to install the library. This type of installation is recommended when using frameworks like React, Vue, or Angular. 

Place the provided dist folder into the root directory where the package.json resides and use the following command to install the library.
```
npm install lib
```
Once you have installed the library via npm, use import to use the library.
```
import { PeformanceSupportXAPI } from "veracity";

```
## Launching Configuration
When launching content from the LMS, the LMS will be configured to perform launch using “TinCan Launch”.  In this case, the content does not need to form the xAPI Actor or hardcode the LRS credentials.  This information is provided to the content as part of launch.  The content SHALL use the information provided via launch over any locally configured information. 

For xAPI content that is web-based and that requires testing with an LRS, you’ll need to create your own credentials using the configuration object below. This configuration object will be passed to the constructor of the ELearningXAPI object. See the "Instantiating the library" section below. 

```
// create credentials configuration object
// auth format : "Basic " + encode(username+":"+password)
const config = {
  "endpoint": "https://lrs.adlnet.gov/xapi/",
  "auth": "Basic " + base64.encode("sicukj:rewriu"), 
  "actor": {
    "name": "Valerie Verifierwicz",
    "account": { homePage: "https://edipi.navy.mil", name: "1625378415" },
    "objectType": "Agent"
  },
  "platform": "Moodle 3.8.3"
};
```
Properties of the configuration object.

| Name | Type | Description |
| ------------| :-----------: | :----- |
| endpoint | string | The Endpoint URL to the LRS’s xAPI (e.g. https://lrs.navy.mil/xAPI) |
| auth     | string | A limited scope, one time use value of the authorization header used to access to the xAPI Endpoint (e.g. Basic dG9tOjEyMzQ=) |
| actor    | JSON Object | The JSON representation of the Actor Object is required in every xAPI Statement sent by LRP. In this document, the information returned from the LRS is referenced in this document as the learner Agent Object. (e.g. {"name":"Valerie Verifierwicz", "objectType":"Agent","account":{"homePage":"https://edipi.navy.mil","name":"0123456789"}) |

## Use

Throughout this document you'll see the use of utility functions like addExtensions or createParent to help with the development of your application. The PerformanceSupportXAPI library also offers constants to help speed up your development. A Reference of the PerformanceSupportXAPI constants can be found in the following sections.  

## **Instantiating the library** 

### **PerformanceSupportXAPI( config )**
To use the Performance Support library, instantiate a PerformanceSupportXAPI object. 

#### **Arguments**
config (Object) : A configuration object based on the configuration credentials provided by a NETC LRS Administrator. A config object is optional and mostly used for development, but can be used in various NETC environments (e.g., development, production, etc.). If a config object isn't passed into the PeformanceSupportXAPI library then the library will perform a launch using "TinCan Launch". 

#### **Returns**
{}: Returns new instance of the PeformanceSupportXAPI library.

**Example**

```
// Create PerformanceSupportXAPI instance. 
var performanceSupportLib = new PerformanceSupportXAPI();

OR 

// Create PerformanceSupportXAPI instance using a config object. 
var performanceSupportLib = new PerformanceSupportXAPI(config);
```
***

## **Utility Functions**
## **Extensions**
After instantiating the PeformanceSupportXAPI library, the next step is to add extensions.

### **Adding Extensions**
When used as part of Context, the Extensions Object provides a way to optionally extend xAPI Statements by including custom properties for a specialized purpose. The values of Extensions can be any value or JSON Object.

Use addExtensions to add a single extension or an object of extensions.
### **PerformanceSupportXAPI.addExtensions( key, value )**
OR
### **PerformanceSupportXAPI.addExtensions( extensions )**


#### **Arguments**
key (String) : An IRI that helps define a specialized purpose.

value (*): Any JSON value or data structure..

or

extensions (Object) : An object containing IRI(s) and their values.

**Example**

```
// add a single extension
performanceSupportLib.addExtensions(
  "https://w3id.org/xapi/netc/extensions/school-center",
  "Naval Training Service Command (NTSC)"
);

// or pass in an object of extensions
const extensions = {
  "https://w3id.org/xapi/netc/extensions/school-center":
    "Naval Training Service Command (NTSC)",
};

performanceSupportLib.addExtensions(extensions);

// or use the provided constants
const { SCHOOL_CENTER } = performanceSupportLib.EXTENSIONS
const extensions = {
  [SCHOOL_CENTER]: "Department of Defense (DOD)",
};

performanceSupportLib.addExtensions(extensions);
```
Listed below are the current library extensions available.

| Key Name      | Key Value                                          | Value Description                                                                       |
|:--------------|:---------------------------------------------------|:----------------------------------------------------------------------------------------|
| SCHOOL_CENTER |https://w3id.org/xapi/netc/extensions/school-center |A string value describing the NETC schoolhouse or learning center that owns the content. |

***
## **Creating a Parent Object**
A parent object is used to identify the activity which contains the current activity, such as the activity of the SCO that contains an assessment, interaction, or objective. Throughout this document you'll see instances where a parent object is created and used within a PerformanceSupportXAPI method. In those cases, a parent is created if an Activity has a direct relation to the Activity which is the Object of the Statement. 

### **PerformanceSupportXAPI.createParent( id, name, description )**
Use createParent to build a parent object.

#### **Arguments**
id (String) : A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the activity which contains the current activity.

description (String): A description of the activity which contains the current activity.

#### **Returns**
{}: Returns a Parent object. 

**Example**

```
// create a parent checklist object. This object will be the parent of a checklist item
const parent = PerformanceSupportXAPI.createParent(
  "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38"
  "Advanced Airborne Sensor Maintenance Checklist",
  "A maintenance checklist for the Advanced Airborne Sensor on the P-8"
  );
```
***
## **PerformanceSupportXAPI Constants**
Throughout this document you'll see PerformanceSupportXAPI constants being used along with the PerformanceSupportXAPI methods.
PerformanceSupportXAPI constants are used to help with development of your application.

### **PerformanceSupportXAPI.ACTIVITY**
**Example**

```
// accessed the checklist item constant
PerformanceSupportXAPI.ACTIVITY.CHECKLIST_ITEM;

OR

// deconstruct the ACTIVITY object and grab the constants you need
const { APPLICATION, CHECKLIST, CHECKLIST_ITEM, IMAGE } = PerformanceSupportXAPI.ACTIVITY;
```

Below is a table listing all available Activity constants.

| Activity Types | Description                                                                                                                           |
| :------------- | :------------------------------------------------------------------------------------------------------------------------------------ |
| APPLICATION    | Any software application that performs specific functions.                                                                            |
| CHECKLIST      | A list of tasks to be completed.                                                                                                      |
| CHECKLIST_ITEM | An individual item contained in a checklist.                                                                                          |
| IMAGE          | A digital representation of a resource such as a photograph, diagram, or illustration.                                                |
| MENU           | A list of actionable options in a software interface presented to a user. A menu can be an entire user interface or only part of one. |
| MENU_ITEM      | A single option in a software interface’s menu.                                                                                       |
| SEARCH_ENGINE  | A software program that searches a database and gathers and reports information that contains or is related to specified terms.       |
| STEP           | A step is one of several actions that the Actor has to do to achieve something, for instance, a goal or the completion of a task.     |
| PROCEDURE      | A procedure is a sequence of steps that are followed systematically to achieve a task or make a decision.                             |
| FILE           | A file is a digital resource for storing information in a specific format on a computer.                                              |
| LINK           | A link refers to a web-based hyperlink intentionally made in the content to a resource.                                               |
| PAGE           | The representation of a specific page number and the information on that page which can be displayed on a screen at one time.         |

***
## **Initializing an Application**
Performance support applications must be both Initialized and Terminated to convey an opening and closing of the application.

### **PerformanceSupportXAPI.initialize( id, name, description )**
Use initialize to send a "initialized" statement to the configured LRS.

#### **Arguments**
id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

#### **Returns**
{}: Returns a Promise. On success the promise will resolve an empty object else reject with an HTTP error code. 

Error codes can be found here https://github.com/adlnet/xAPI-Spec/blob/master/xAPI-Communication.md#errorcodes

**Example**

```
async function myApplication() {
  let result = await performanceSupportLib.initialize(
    "https://navy.mil/netc/xapi/activities/applications/60608f6b-c938-4ae7-b170-816fdf5947da",
    "Advanced Airborne Sensor Performance Support App",
    "A mobile application used at the moment of need for performance support of the Advanced Airborne Sensor on the P-8."
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Terminating an Application**
A Terminated Statement is required to be communicated at the end of a user’s session with a performance support application.

### **PerformanceSupportXAPI.terminate( id, name, description )**
Use terminate to send a "terminated" statement to the configured LRS.

#### **Arguments**
id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  let result = await performanceSupportLib.terminate(
    "https://navy.mil/netc/xapi/activities/applications/60608f6b-c938-4ae7-b170-816fdf5947da",
    "Advanced Airborne Sensor Performance Support App",
    "A mobile application used at the moment of need for performance support of the Advanced Airborne Sensor on the P-8."
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Selecting a Checklist Item**
A performance support application can send xAPI Statements to indicate when checklist items within a checklist have been selected or deselected. A checklist is often used to help people follow a policy or process correctly.


### **PerformanceSupportXAPI.selected( type, value, id, name, description, parent )**
Use selected to send a checklist item "selected" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating either a menu or checklist item is the selected or deselected object.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Examples**

```
async function myApplication() {
  // grab CHECKLIST_ITEM constant
  const { CHECKLIST_ITEM } = performanceSupportXAPI.ACTIVITY;

  // create parent of the checklist item which is a checklist
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
    "Advanced Airborne Sensor Maintenance Checklist",
    "A maintenance checklist for the Advanced Airborne Sensor on the P-8"
  );

  // send selected statement that has a parent
  let result = await performanceSupportLib.selected(
    CHECKLIST_ITEM,
    "https://navy.mil/netc/xapi/activities/checklist-items/f8be33ad-cb1b-480c-ae91-4444114ba065",
    "Cleaned and Inspected Fiber Cables and Umbilicals",
    "Checklist item representing the procedure to perform cleaning and inspection of fiber cables and umbilicals",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Deselecting a Checklist Item**
A performance support application can send xAPI Statements to indicate when checklist items within a checklist have been selected or deselected. A checklist is often used to help people follow a policy or process correctly.
checklist item.

### **PerformanceSupportXAPI.deselected( type, value, id, name, description, parent )**
Using deselected will send a "deselected" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating either a menu or checklist item is the selected or deselected object.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Examples**

```
async function myApplication() {
  // grab CHECKLIST_ITEM constant
  const { CHECKLIST_ITEM } = performanceSupportLib.ACTIVITY;

  // create parent of the checklist item which is a checklist
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
    "Advanced Airborne Sensor Maintenance Checklist",
    "A maintenance checklist for the Advanced Airborne Sensor on the P-8"
  );

  // send selected statement that has a parent
  let result = await performanceSupportLib.deselected(
    CHECKLIST_ITEM,
    "https://navy.mil/netc/xapi/activities/checklist-items/f8be33ad-cb1b-480c-ae91-4444114ba065",
    "Cleaned and Inspected Fiber Cables and Umbilicals",
    "Checklist item representing the procedure to perform cleaning and inspection of fiber cables and umbilicals",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```

## **Completing a Checklist**

The performance support application can send xAPI Statements to indicate when the checklist has been completed. The completion criteria for a checklist is determined by the design and purpose of the checklist, which could require that all checklist items be selected before the checklist is considered to be completed.

**Example**

### **PerformanceSupportXAPI.completedChecklist( id, name, description )**
Use completedChecklist to send a "completed" statement to the configured LRS.

#### **Arguments**
id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  let result = await performanceSupportLib.completedChecklist(
    "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
    "Advanced Airborne Sensor Maintenance Checklist",
    "A maintenance checklist for the Advanced Airborne Sensor on the P-8."

  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
*** 
## **Step-by-step Procedures** 
Step-by-step procedures used as performance support can help people review how to perform a task. A procedure is a sequence of steps that are followed systematically to achieve a task or make a decision. A step contains directions or tasks that are done in the same way every time. In order to associate which steps belong to a procedure, content developers SHALL use the contextActivities object to indicate a parent relationship between the steps and the overall procedure.

### **PerformanceSupportXAPI.viewed( value, id, name, description, parent )**
Use viewed to indicate a step or procedure has been performed. Using viewed will send a "viewed" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating either a Step, or Procedure has been viewed.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

### **Viewing a Step**

**Example**

```
async function myApplication() {
  // grab STEP constant from the ACTIVITY object
  const { STEP } = performanceSupportLib.ACTIVITY;

  // create parent for step 1 which is a procedure
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
    "Auxiliary Equipment Maintenance Procedure",
    "A maintenance procedure required to ensure proper auxiliary equipment operation."
  );

  let result = await performanceSupportLib.viewed(
    STEP,
    "https://navy.mil/netc/xapi/activities/steps/382927d2-b0e9-11ea-b3de-0242ac130004",
    "Step 1",
    "Clean the auxiliary equipment",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Viewing a Procedure**

**Example**
```
async function myApplication() {
  // grab PROCEDURE constant from the ACTIVITY object
  const { PROCEDURE } = performanceSupportLib.ACTIVITY;

  let result = await performanceSupportLib.viewed(
    PROCEDURE,
    "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
    "Auxiliary Equipment Maintenance Procedure",
    "A maintenance procedure required to ensure proper auxiliary equipment operation."
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Searching for Terms**
Performance support applications often implement search engines or some type of search functionality over the performance support content or data. This capability provides the user with the ability to quickly find information they are looking for either within the performance support app or in externally searched resources. The NETC Performance Support Profile defines how to track the user-entered search terms in order to provide insights and analytics on what the users commonly search.

### **PerformanceSupportXAPI.searched( value, id, name, description )**
Use searched to send a "searched" statement to the configured LRS. The search term value will be set to the result.response property of the statement.

#### **Arguments**
value (String) : The search term entered by the user.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  let result = await performanceSupportLib.searched(
    "SMPDM",
    "https://navy.mil/netc/xapi/activities/search-engine/489e9c1b-13ef-43ae-a7ab-ffe138c19f77",
    "Application Search",
    "A search Activity performed using the application’s user interface."
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();
```
***
## **Selecting a Search Result**
After a user searches for a term in a performance support application, the search engine typically returns several results to be displayed to the user. Each of the results are linked to either an internal or external resource related to the search term. The NETC Performance Support Profile tracks which results are selected by the user in order to provide insights and analytics on what the users selected from all search results. The linked search result selected by the user SHALL be indicated in result.response of the xAPI Searched Statement.

### **PerformanceSupportXAPI.selected( type, value, id, name, description, parent )**
Use selected to send a search result "selected" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating either a menu or checklist item is the selected or deselected object.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Examples**

```
async function myApplication() {
  // grab LINK constant
  const { LINK } = performanceSupportLib.ACTIVITY;

  // create a parent object for the selected link
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/search-engine/489e9c1b-13ef-43ae-a7ab-ffe138c19f77",
    "Application Search Engine",
    "The search engine used in the performance support application."
  );

  // send selected statement that has a parent
  let result = await performanceSupportLib.selected(
    LINK,
    "https://navy.mil/netc/xapi/activities/links/e4042887-6035-41bb-ae98-96d9f6e11884",
    "SMPDM Link",
    "A link to a resource on SMPDM.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***

## **Menus & Menu Items - Accessed Menu Item**
User interface activities such as interacting with menu items can also be tracked within performance support content. The requirements and examples for menu activities are defined in the NETC Common Reference Profile since they can be optionally tracked in many types of content. However, there is an additional NETC Performance Support Profile requirement to add a context grouping relationship with the performance support application.

### **PerformanceSupportXAPI.accessed( type, value, id, name, description, parent )**
Use accessed to send a menu or menu item "accessed" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating either a Menu or Menu Item has been accessed.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  const { MENU_ITEM } = performanceSupportLib.ACTIVITY;

  // create a parent object for the selected link
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/menus/25fcb754-b09e-11ea-b3de-0242ac130004",
    "Configuration Menu",
    "The configuration menu provided for the Advanced Airborne Sensor App."
  );

  let result = await performanceSupportLib.accessed(
    MENU_ITEM
    "https://navy.mil/netc/xapi/activities/menu-items/6ac32904-0774-49d8-9527-ccfb264cce03",
    "AAS Configuration",
    "A submenu item in the Configuration Menu.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Like & Dislike Buttons**
The NETC Common Reference Profile enables content to track which things are liked or disliked the most by the users in order to provide quantitative insights and analytics. The same xAPI Statements for liking and disliking something can also be used to represent favoriting or unfavoriting. All of the NETC xAPI requirements for a Liked or Disliked Statement are provided in the NETC Common Reference Profile.

### **PerformanceSupportXAPI.liked( type, id, name, description, parent )**
Use liked to send a "liked" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating an Activity type has been liked.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  const { IMAGE } = performanceSupportLib.ACTIVITY;

  // create a parent object for the liked image
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
    "AAR Resources Page",
    "A resource page within the Advanced Airborne Sensor Performance Support App."
  );

  let result = await performanceSupportLib.liked(
    IMAGE
    "https://navy.mil/netc/xapi/activities/images/326b1af6-b0a0-11ea-b3de-0242ac130004",
    "SMP Inspection Diagram",
    "A diagram of SMP Avionics and Power Umbilical Inspection Setup.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***

### **PerformanceSupportXAPI.disliked( type, id, name, description, parent )**
Use disliked to send a "disliked" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating an Activity type has been disliked.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  const { IMAGE } = performanceSupportLib.ACTIVITY;

  // create a parent object for the liked image
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
    "AAR Resources Page",
    "A resource page within the Advanced Airborne Sensor Performance Support App."
  );

  let result = await performanceSupportLib.liked(
    IMAGE
    "https://navy.mil/netc/xapi/activities/images/326b1af6-b0a0-11ea-b3de-0242ac130004",
    "SMP Inspection Diagram",
    "A diagram of SMP Avionics and Power Umbilical Inspection Setup.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Files, Links, and Pages**
Activities such as files, links, and pages can be tracked within performance support content. The requirements and examples for these Activity Types are defined in the NETC Common Reference Profile. However, there is an additional NETC Performance Support Profile requirement to add a context grouping relationship with the performance support application.

### **PerformanceSupportXAPI.opened( type, id, name, description, parent )**
Use opened to send an "opened" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating an Activity type has been opened.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  const { FILE } = performanceSupportLib.ACTIVITY;

  // create a parent object for the opened file. The file was open on a resource page.
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
    "AAR Resources Page",
    "A resource page within the Advanced Airborne Sensor Performance Support App."
  );

  let result = await performanceSupportLib.opened(
    FILE
    "https://navy.mil/netc/xapi/activities/files/307325ca-36f4-49cb-8787-80faf527f042",
    "Advanced Airborne Sensor Special Mission Pod Deployment Mechanism Technical Manual, 
     Aircraft Maintenance Manual (AAM) CDRL C027: 20 October 2015, D809-04231-1",
    "A PDF file tech manual for the Advanced Airborne Sensor.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
### **PerformanceSupportXAPI.closed( type, id, name, description, parent )**
Use closed to send an "closed" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating an Activity type has been closed.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  const { FILE } = performanceSupportLib.ACTIVITY;

  // create a parent object for the closed file. The file was closed on a resource page.
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
    "AAR Resources Page",
    "A resource page within the Advanced Airborne Sensor Performance Support App."
  );

  let result = await performanceSupportLib.closed(
    FILE
    "https://navy.mil/netc/xapi/activities/files/307325ca-36f4-49cb-8787-80faf527f042",
    "Advanced Airborne Sensor Special Mission Pod Deployment Mechanism Technical Manual, 
     Aircraft Maintenance Manual (AAM) CDRL C027: 20 October 2015, D809-04231-1",
    "A PDF file tech manual for the Advanced Airborne Sensor.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
### **PerformanceSupportXAPI.printed( type, id, name, description, parent )**
Use printed to send an "printed" statement to the configured LRS.

#### **Arguments**
type (String) : A string indicating an Activity type (FILE, IMAGE, PAGE) has been printed.

id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

parent (Object) : An Activity with a direct relation to the Activity which is the Object of the Statement.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**

```
async function myApplication() {
  const { IMAGE } = performanceSupportLib.ACTIVITY;

  // create a parent object for the closed file. The file was closed on a resource page.
  const parent = performanceSupportLib.createParent(
    "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
    "AAR Resources Page",
    "A resource page within the Advanced Airborne Sensor Performance Support App."
  );

  let result = await performanceSupportLib.closed(
    IMAGE
    "https://navy.mil/netc/xapi/activities/images/326b1af6-b0a0-11ea-b3de-0242ac130004",
    "SMP Inspection Diagram",
    "A diagram of SMP Avionics and Power Umbilical Inspection Setup.",
    parent
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```
***
## **Uploading a File**
In some cases an Attachment is logically an important part of a Learning Record. It could be an essay, a video, etc. Another example of such an Attachment is (the image of) a certificate that was granted as a result of an experience. It is useful to have a way to store these Attachments in and retrieve them from an LRS.

### **PerformanceSupportXAPI.attachment( value, usageType, contentType, display, description )**

Use attachment to build an attachment object.

#### **Arguments**
value (string) :  An array buffer containing the data to post. 

usageType (string) : Identifies the usage of this attachment.

contentType (string) : The content type of the attachment.

display (string) Display name (title) of this attachment. 

description (string) A description of the attachment.

**Example**

```
  let arrayBuffer = null;
  const reader = new FileReader();  

  reader.onload = async function () {
    arrayBuffer = this.result;
  };
  // some file you upload through a input file field
  reader.readAsArrayBuffer(inputFile.files[0]);

  const attachment = performanceSupportLib.attachment(
    arrayBuffer,
    "image",
    "image/jpeg",
    "Image Attachment.",
    "Uploading An Image Attachment."
  );
```

### **PerformanceSupportXAPI.uploaded( id, name, description, attachment )** 
Use uploaded to send an "uploaded" statement to configured LRS and to save an attachment to the LRS.

#### **Arguments**
id (String) : A unique identifier for the interaction Activity according to the Activity ID requirements in the NETC Common Reference Profile

name (String): A value that represents the official name or title of the Activity which contains the current Activity.

description (String): A description of the Activity which contains the current Activity.

attachment (Object) : An attachment object.

#### **Returns**
{}: Returns a Promise. The promise will resolve an empty object.

**Example**.

```
async function myApplication() {

  let result = await performanceSupportLib.uploaded(
    "https://navy.mil/netc/xapi/activities/files/c132acb1-c4e1-4b7c-9798-badc4a587561",
    "Airborne Sensor Notes" 
    "A text file of personal notes taken while training on the Airborne Sensor Lesson."
    attachment
  )
  .catch( error => console.log(`An error has occurred ${error}`));
}

myApplication();

```